# Parcel Scanner
Simple blazor based app for crossdocking package reception.
SQLite is used as data store.

## Outbound
*Parcels from client to customers*

User selects list of outbound packages (created over API by external app). 
Every scanned barcode is checked against the list and all discrepancies must be manually reviewed.

## Returns
*Undeliverable/refused/returnen parcels*

User creates new list. All collected barcodes are stored localy in SQLite database.
Once completed, the list can be accessed (over API) by extenal app.

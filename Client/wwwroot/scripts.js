﻿window.showConfirmation = function () {
    return "You have unsaved changes. Are you sure you want to leave?";
};
window.addEventListener("load", function () {
    AOS.init();
});
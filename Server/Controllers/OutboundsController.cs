﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SwP.Fulfillment.ParcelLogger.Shared;
using Newtonsoft.Json;

namespace SwP.Fulfillment.ParcelLogger.Server.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OutboundsController : ControllerBase
    {
        private readonly DbStorageContext _context;   
        private readonly ILogger<OutboundsController> _logger;
    
        public OutboundsController(ILogger<OutboundsController> logger, DbStorageContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<OutboundList[]>> Get()
        {
            _logger.LogDebug("List requested");
            try
            {
                var result = await _context.Outbound.Include(x => x.Records).ToListAsync();//.Include(x => x.Records).ToListAsync();
                if (result == null) return NotFound();
                return new JsonResult(result, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore});
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Exception detected");
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data");
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<OutboundList>> Get(int id)
        {
            var result = await _context.Outbound.Include(x => x.Records).FirstAsync(x => x.Id == id);
            //var record = _context.Batches.Find(x => x.Id == id);
            if (result == null) return NotFound();
            return new JsonResult(result, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

        }

        [HttpPost]
        public async Task<ActionResult<OutboundList>> Post([FromBody] OutboundList recordsList)
        {
            _logger.LogDebug("Saving new batch");
           _context.Outbound.Add(recordsList);
            await _context.SaveChangesAsync();

            //return CreatedAtAction(nameof(OutboundList), new { id = recordsList.Id }, recordsList);
            return NoContent();
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] OutboundList b)
        {
            if (b == null)
            {
                return BadRequest(nameof(OutboundList) + "is Null");
            }

            var batch = await _context.Outbound.Include(x => x.Records).FirstOrDefaultAsync(x => x.Id == b.Id);

            if (batch == null)
            {
                return NotFound();
            }

            // Update parent
            _context.Entry(batch).CurrentValues.SetValues(b);
            // Delete children
            foreach (var existingRecord in batch.Records.ToList())
            {
                if (!b.Records.Any(c => c.Id == existingRecord.Id))
                    _context.Records.Remove(existingRecord);
            }

            // Update and Insert children
            foreach (var r in b.Records)
            {
                r.OutboundID = b.Id;
                var existingChild = batch.Records
                    .Where(c => c.Id == r.Id && c.Id != default)
                    .SingleOrDefault();

                if (existingChild != null)
                    // Update child
                    _context.Entry(existingChild).CurrentValues.SetValues(r);
                else
                {
                    // Insert child
                    batch.Records.Add(r);
                }
            }
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error saving data");
            }

            return Ok(batch);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            _logger.LogDebug("Deleting batch with id " + id);
            try
            {
                var batch = await _context.Outbound.FirstOrDefaultAsync(x => x.Id == id);
                if (batch != null)
                {
                    _context.Outbound.Remove(batch);
                    var result = await _context.SaveChangesAsync();
                    return NoContent();
                }
                return NotFound();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Captured exception");
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data");
            }
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SwP.Fulfillment.ParcelLogger.Shared;

namespace SwP.Fulfillment.ParcelLogger.Server.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RecordsController : ControllerBase
    {

        private readonly DbStorageContext _context;
        private readonly ILogger<RecordsController> _logger;

        public RecordsController(ILogger<RecordsController> logger, DbStorageContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<Record[]>> Get()
        {
            _logger.LogDebug("List of batches requested");
            try
            {
                var result = await _context.Records.ToListAsync();
                if (result == null) return NotFound();
                return Ok(result);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Captured exception");
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data");
            }
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<Record>> Get(int id)
        {
            var record = await _context.Records.FirstOrDefaultAsync(x => x.OutboundID == id);

            if (record == null)
            {
                return NotFound();
            }

            return Ok(record);
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Record r)
        {
            _logger.LogDebug("Saving new record");
            try
            {
                r.DateCreated = DateTime.Now;
               // r.Batch = null;
                _context.Records.Add(r);

                var result = await _context.SaveChangesAsync();
                if (result != 1) return NotFound();
                return Ok(r);
                //return CreatedAtAction(nameof(GetRecord), new { id = newRecord.Id }, newRecord);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Exception thrown");
                return StatusCode(StatusCodes.Status500InternalServerError, "Error saving data");
            }
        }


        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] Record r)
        {
            if (r == null)
            {
                return BadRequest(nameof(Record) + "is Null");
            }

            var record = await _context.Records.FirstOrDefaultAsync(x => x.Id == r.Id);

            if (record == null)
            {
                return NotFound();
            }

            // Update the properties of the existing record with the properties from the updated record
            record.DateCreated = r.DateCreated;
            record.Barcode = r.Barcode;
            record.OutboundID = r.OutboundID;
            await _context.SaveChangesAsync();

            return Ok(record);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            _logger.LogDebug("Deleting batch with id " + id);
            try
            {
                var record = await _context.Records.FirstOrDefaultAsync(x => x.Id == id);
                if (record != null)
                {
                    _context.Records.Remove(record);
                    var result = await _context.SaveChangesAsync();
                    if (result != 1) return NotFound();
                }
                return Ok(record.Id);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Captured exception");
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data");
            }
        }
    }
}

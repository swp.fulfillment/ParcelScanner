﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwP.Fulfillment.ParcelLogger.Shared
{
    public class DbStorageContext : DbContext
    {
        public DbStorageContext(DbContextOptions<DbStorageContext> options) : base(options) { }

        public DbSet<Record> Records { get; set; }
        public DbSet<OutboundList> Outbound { get; set; }
        public DbSet<ReturnedList> Returned { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            //modelBuilder.Entity<Record>()
            //    .HasOne(r => r.Outbound)
            //    .WithMany(rl => rl.OutboundRecords)
            //    .HasForeignKey(rl => rl.OutboundID)
            //    .HasPrincipalKey(rl => rl.ListID);

            //modelBuilder.Entity<Record>()
            //    .HasOne(r => r.Returned)
            //    .WithMany(rl => rl.ReturnedRecords)
            //    .HasForeignKey(rl => rl.ReturnedID).OnDelete(DeleteBehavior.NoAction)
            //    .HasPrincipalKey(rl => rl.ListID);

            modelBuilder.Entity<Record>()
                .HasOne(r => r.Outbound)
                .WithMany(rl => rl.Records)
                .HasForeignKey(r => r.OutboundID) // Use the appropriate foreign key property
                .HasPrincipalKey(rl => rl.Id);

            modelBuilder.Entity<Record>()
                .HasOne(r => r.Returned)
                .WithMany(rl => rl.Records)
                .HasForeignKey(r => r.ReturnedID) // Use the appropriate foreign key property
                .OnDelete(DeleteBehavior.SetNull)
                .HasPrincipalKey(rl => rl.Id);

        }
    }
}

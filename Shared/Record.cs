﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace SwP.Fulfillment.ParcelLogger.Shared
{

    public class Record
    {

        [Required] 
        [Key]
        public int Id { get; set; }
        
        [JsonIgnore] 
        //[Required]
        public int? OutboundID { get; set; }
     
        [Required]
        public DateTime DateCreated { get; set; }

        [Required(ErrorMessage ="At least one barcode must be provided")]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "Barcode should be between 5 and 50 characters.")]
        public string Barcode { get; set; } = string.Empty;

        //Optional properties

        [JsonIgnore]
        public int? ReturnedID { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessage = "Barcode should be between 5 and 50 characters.")] 
        public string? Barcode2 { get; set; }

        [StringLength(50, MinimumLength = 5, ErrorMessage = "Barcode should be between 5 and 50 characters.")]
        public string? Barcode3 { get; set; }

        public DateTime? DateReceivedToReturn { get; set; }
        public DateTime? DateReceivedToSent { get; set; }

        // Navigation properties
        [JsonIgnore]
        public virtual OutboundList? Outbound { get; set; }
        [JsonIgnore]
        public virtual ReturnedList? Returned { get; set; }

        //ctor
        public Record()
        {
           // Outbound = new OutboundList();
        }

    }

}

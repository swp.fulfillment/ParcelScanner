﻿using System.ComponentModel.DataAnnotations;

namespace SwP.Fulfillment.ParcelLogger.Shared
{
    public class RecordsList
    {
        [Required]
        [Key]
        public int Id { get; set; }
        [Required]
        public DateTime DateCreated { get; set; }
        [Required(ErrorMessage ="Provide name for this list")]
        [StringLength(50, MinimumLength = 1)]
        public string Description { get; set; } = string.Empty;
        public bool Locked { get; set; } = false;
        public bool Edited { get; set; } = false;

        public RecordsList() 
        {
            Records = new HashSet<Record>();
        }

        //Navigation properties
        public virtual ICollection<Record> Records { get; set; }
    }

    public class OutboundList : RecordsList { }
    public class ReturnedList : RecordsList { }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SwP.Fulfillment.ParcelLogger.Shared.Migrations
{
    /// <inheritdoc />
    public partial class NewModels : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Records_Batches_BatchID",
                table: "Records");

            migrationBuilder.DropTable(
                name: "Batches");

            migrationBuilder.RenameColumn(
                name: "DateTime",
                table: "Records",
                newName: "Created");

            migrationBuilder.RenameColumn(
                name: "Confirmed",
                table: "Records",
                newName: "ReturnedID");

            migrationBuilder.RenameColumn(
                name: "BatchID",
                table: "Records",
                newName: "OutboundID");

            migrationBuilder.RenameIndex(
                name: "IX_Records_BatchID",
                table: "Records",
                newName: "IX_Records_OutboundID");

            migrationBuilder.AddColumn<string>(
                name: "Barcode2",
                table: "Records",
                type: "TEXT",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Barcode3",
                table: "Records",
                type: "TEXT",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ReceivedToReturn",
                table: "Records",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ReceivedToSent",
                table: "Records",
                type: "TEXT",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "RecordsList",
                columns: table => new
                {
                    ListID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Description = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
                    DateTime = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Locked = table.Column<bool>(type: "INTEGER", nullable: false),
                    Edited = table.Column<bool>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RecordsList", x => x.ListID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Records_ReturnedID",
                table: "Records",
                column: "ReturnedID");

            migrationBuilder.AddForeignKey(
                name: "FK_Records_RecordsList_OutboundID",
                table: "Records",
                column: "OutboundID",
                principalTable: "RecordsList",
                principalColumn: "ListID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Records_RecordsList_ReturnedID",
                table: "Records",
                column: "ReturnedID",
                principalTable: "RecordsList",
                principalColumn: "ListID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Records_RecordsList_OutboundID",
                table: "Records");

            migrationBuilder.DropForeignKey(
                name: "FK_Records_RecordsList_ReturnedID",
                table: "Records");

            migrationBuilder.DropTable(
                name: "RecordsList");

            migrationBuilder.DropIndex(
                name: "IX_Records_ReturnedID",
                table: "Records");

            migrationBuilder.DropColumn(
                name: "Barcode2",
                table: "Records");

            migrationBuilder.DropColumn(
                name: "Barcode3",
                table: "Records");

            migrationBuilder.DropColumn(
                name: "ReceivedToReturn",
                table: "Records");

            migrationBuilder.DropColumn(
                name: "ReceivedToSent",
                table: "Records");

            migrationBuilder.RenameColumn(
                name: "ReturnedID",
                table: "Records",
                newName: "Confirmed");

            migrationBuilder.RenameColumn(
                name: "OutboundID",
                table: "Records",
                newName: "BatchID");

            migrationBuilder.RenameColumn(
                name: "Created",
                table: "Records",
                newName: "DateTime");

            migrationBuilder.RenameIndex(
                name: "IX_Records_OutboundID",
                table: "Records",
                newName: "IX_Records_BatchID");

            migrationBuilder.CreateTable(
                name: "Batches",
                columns: table => new
                {
                    BatchID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DateTime = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Edited = table.Column<bool>(type: "INTEGER", nullable: false),
                    Locked = table.Column<bool>(type: "INTEGER", nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Batches", x => x.BatchID);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Records_Batches_BatchID",
                table: "Records",
                column: "BatchID",
                principalTable: "Batches",
                principalColumn: "BatchID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

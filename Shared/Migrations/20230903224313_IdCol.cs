﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SwP.Fulfillment.ParcelLogger.Shared.Migrations
{
    /// <inheritdoc />
    public partial class IdCol : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ListID",
                table: "Returned",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "RecordID",
                table: "Records",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "ListID",
                table: "Outbound",
                newName: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Returned",
                newName: "ListID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Records",
                newName: "RecordID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Outbound",
                newName: "ListID");
        }
    }
}

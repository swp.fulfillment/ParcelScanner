﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SwP.Fulfillment.ParcelLogger.Shared.Migrations
{
    /// <inheritdoc />
    public partial class GptiedModel : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Records_RecordsList_OutboundID",
                table: "Records");

            migrationBuilder.DropForeignKey(
                name: "FK_Records_RecordsList_ReturnedID",
                table: "Records");

            migrationBuilder.DropTable(
                name: "RecordsList");

            migrationBuilder.RenameColumn(
                name: "ReceivedToSent",
                table: "Records",
                newName: "DateReceivedToSent");

            migrationBuilder.RenameColumn(
                name: "ReceivedToReturn",
                table: "Records",
                newName: "DateReceivedToReturn");

            migrationBuilder.RenameColumn(
                name: "Created",
                table: "Records",
                newName: "DateCreated");

            migrationBuilder.AlterColumn<int>(
                name: "ReturnedID",
                table: "Records",
                type: "INTEGER",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<string>(
                name: "Barcode",
                table: "Records",
                type: "TEXT",
                maxLength: 50,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "Outbound",
                columns: table => new
                {
                    ListID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DateCreated = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Description = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    Locked = table.Column<bool>(type: "INTEGER", nullable: false),
                    Edited = table.Column<bool>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Outbound", x => x.ListID);
                });

            migrationBuilder.CreateTable(
                name: "Returned",
                columns: table => new
                {
                    ListID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DateCreated = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Description = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    Locked = table.Column<bool>(type: "INTEGER", nullable: false),
                    Edited = table.Column<bool>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Returned", x => x.ListID);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Records_Outbound_OutboundID",
                table: "Records",
                column: "OutboundID",
                principalTable: "Outbound",
                principalColumn: "ListID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Records_Returned_ReturnedID",
                table: "Records",
                column: "ReturnedID",
                principalTable: "Returned",
                principalColumn: "ListID",
                onDelete: ReferentialAction.SetNull);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Records_Outbound_OutboundID",
                table: "Records");

            migrationBuilder.DropForeignKey(
                name: "FK_Records_Returned_ReturnedID",
                table: "Records");

            migrationBuilder.DropTable(
                name: "Outbound");

            migrationBuilder.DropTable(
                name: "Returned");

            migrationBuilder.RenameColumn(
                name: "DateReceivedToSent",
                table: "Records",
                newName: "ReceivedToSent");

            migrationBuilder.RenameColumn(
                name: "DateReceivedToReturn",
                table: "Records",
                newName: "ReceivedToReturn");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Records",
                newName: "Created");

            migrationBuilder.AlterColumn<int>(
                name: "ReturnedID",
                table: "Records",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Barcode",
                table: "Records",
                type: "TEXT",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 50);

            migrationBuilder.CreateTable(
                name: "RecordsList",
                columns: table => new
                {
                    ListID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DateTime = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Description = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
                    Edited = table.Column<bool>(type: "INTEGER", nullable: false),
                    Locked = table.Column<bool>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RecordsList", x => x.ListID);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Records_RecordsList_OutboundID",
                table: "Records",
                column: "OutboundID",
                principalTable: "RecordsList",
                principalColumn: "ListID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Records_RecordsList_ReturnedID",
                table: "Records",
                column: "ReturnedID",
                principalTable: "RecordsList",
                principalColumn: "ListID");
        }
    }
}
